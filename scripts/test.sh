#!/bin/bash

# Run workflow
java -jar /cromwell/cromwell-47.jar run https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/wdl-pipeline/alignment.wdl --inputs /inputs/bco-app-a-thon-inputs.json 2>&1 | tee -a cromwell.log

if grep -q "workflow finished with status 'Succeeded'" cromwell.log ; then

		echo "Analysis completed"

		# Extract json

		cat cromwell.log | awk '{if (/SingleWorkflowRunnerActor workflow finished with status/) {start = "true"} else if (start == "true" && end != "true") {print $0; if ($0=="}") {end = "true"}}}' > output.json
		
		# Move outputs to proper directories
		#### Quality check
		mkdir -p /results/fastqc/
		for i in `jq -r '.outputs."alignment.fastqc_files"' -c output.json | sed 's/\[//' | sed 's/\]//' | sed 's/,/ /g' | sed 's/"//g'`; do 
			mv $i /results/fastqc/; 
		done

		#### Bam
		mkdir -p /results/bam
		mv `jq -r '.outputs."alignment.lane_markdup_bam"' output.json` /results/bam

		#### BCO 
		mkdir -p /results/bco
		mv `jq -r '.outputs."alignment.bco_report_odt"' output.json` /results/bco
		mv `jq -r '.outputs."alignment.bco"' output.json` /results/bco
		mv `jq -r '.outputs."alignment.bco_report_docx"' output.json` /results/bco
		mv `jq -r '.outputs."alignment.bco_report_pdf"' output.json` /results/bco

fi
### bioobject
cpu=$(lscpu | grep '^CPU(s)' | grep -o '[0-9]*')
memory=$(cat /proc/meminfo | grep MemTotal | grep -o '[0-9]*' |  awk '{ print $1/1024/1024 ; exit}')
finishtime=$(date +%s)
starttime=$(cat starttime)
tasktime=$((finishtime-starttime))


os_name=$(cat /etc/os-release | grep -e "^NAME" | grep -o "\".*\"" | sed 's/"//g')
os_version=$(cat /etc/os-release | grep -e "^VERSION" | grep -o "\".*\"" | sed 's/"//g')
jo -p name=$os_name version=$os_version > software_ubuntu.bco
software_prerequisites=$(tools="[]"; for toolfile in $(ls software*.bco); do tools=$(echo $tools | jq ". + [$(cat $toolfile)]") ; done; echo $tools)
external_data_endpoints=$(tools="[]"; for toolfile in $(ls datasource*.bco); do tools=$(echo $tools | jq ". + [$(cat $toolfile)]") ; done; echo $tools)

step=$(jo name="$task_name" version="$task_version")
provenance_domain=$(jo -p \
    steps=$(jo -a $step) \
)

if [[ $task_name == *"quality_check_fastqc"* ]]; then
    step="1"
else
    step="2"
fi
execution_domain=$(jo -p \
  script="https://gitlab.com/intelliseq/workflows/raw/master/src/main/wdl/tasks/$task_name/$task_version/$task_name.wdl" \
  script_driver="shell" \
  software_prerequisites="$software_prerequisites" \
  external_data_endpoints="$external_data_endpoints" \
  step="$step" \
  name="$task_name" \
)



CPU=$(jo param=cpu value=$cpu step=$step name="$task_name")
MEMORY=$(jo param=memory value=$memory step=$step name="$task_name")
TASKTIME=$(jo param=tasktime value=$tasktime step=$step name="$task_name")

parametric_domain=$(jo -a $CPU $MEMORY $TASKTIME)

biocomputeobject=$(jo -p \
  bco_spec_version="https://w3id.org/biocompute/1.3.0/" \
  bco_id="https://intelliseq.com/flow/$(uuidgen)" \
  provenance_domain="$provenance_domain" \
  execution_domain="$execution_domain" \
  parametric_domain="$parametric_domain" \
)

echo "$biocomputeobject" > bco.json
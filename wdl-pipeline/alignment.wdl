# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  Name: Alignment
#  Authors:
#    - https://gitlab.com/marysiaa
#    - https://gitlab.com/marpiech
#    - https://gitlab.com/mremre
#  Copyright: Copyright 2019 Intelliseq
#  Description: >
#    Alignment module performs bwa-mem alignment quality check wich fastqc
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import "https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/wdl-tasks/alignment-bwa-mem.wdl" as alignment_bwa_mem_task
import "https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/wdl-tasks/quality-check-fastqc.wdl" as quality_check_fastqc_task
import "https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/wdl-tasks/merge-bco.wdl" as merge_bco_task
import "https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/wdl-tasks/bco-report.wdl" as bco_report_task

workflow alignment {

  # @Input(name = "Fastqs 1", desc = "Fastq files 1 (paired-end)")
  File fastq_1
  # @Input(name = "Fastqs 2", desc = "Fastq files 2 (paired-end)")
  File fastq_2

  # 1. Run fastqc and quality check reports
  call quality_check_fastqc_task.quality_check_fastqc {
    input:
      fastq_1 = fastq_1,
      fastq_2 = fastq_2
    }
  # 2. Align reads per lane, mark duplicates and sort alignments by coordinates
    call alignment_bwa_mem_task.alignment_bwa_mem {
      input:
        fastq_1 = fastq_1,
        fastq_2 = fastq_2
    }

    call merge_bco_task.merge_bco {
      input:
        bco_fastqc = quality_check_fastqc.bco_fastqc,
        bco_bwa_mem = alignment_bwa_mem.bco_bwa_mem
    }
        
    call bco_report_task.bco_report {
      input:
        bco_json = merge_bco.bco
    }

  output {

    Array[File] fastqc_files = quality_check_fastqc.fastqc_files
    File lane_markdup_bam = alignment_bwa_mem.lane_markdup_bam
    File bco = merge_bco.bco
    File bco_report_pdf = bco_report.bco_report_pdf
    File bco_report_odt = bco_report.bco_report_odt
    File bco_report_docx = bco_report.bco_report_docx
    
  }
}

#!/bin/bash
DIR=$(dirname "$0")

#!/bin/bash

function help() {
  printf "  \n"
  printf "  NAME\n"
  printf "      generate-report\n"
  printf "  \n"
  printf "  SYNPOSIS\n"
  printf "      generate-report.sh --json name1=JSON_FILE1,name2=JSON_FILE2 --template FILE1\n"
  printf "  \n"
  printf "  DESCRIPTION\n"
  printf "  Creates task directory. Creates test directory. Prepares templates\n"
  printf "  \n"
  printf "      -h|--help                      prints help\n"
  printf "      -j|--json                      list of json files with their respective names that will be used \n"
  printf "      -t|--template                   template file \n"
  printf "  \n"
}

export -f help

export SRC_DIR="src/main/wdl/tasks/"
export REPOSITORY="intelliseqngs/"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -h|--help)
    help
    exit 0
    shift # past argument
    ;;
    -j|--json)
    export JSON="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--template)
    export TEMPLATE="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [ -z "$JSON" ]; then
  help
  printf "  ERROR:\n"
  printf "  Json parameter must be specified:\n"
  printf "    -j|--json \"data1=file1.sjon,data2=file2.json\"\n"
  printf "  \n"
  exit
fi

if [ -z "$TEMPLATE" ]; then
  help
  printf "  ERROR:\n"
  printf "  Template parameter must be specified:\n"
  printf "    -t|--template \"path-to-template-file\"\n"
  printf "  \n"
  exit
fi

  SOURCE="${BASH_SOURCE[0]}"

echo "Filling template..."
$DIR/generate-report.py --json $JSON --template $TEMPLATE > content.xml
echo "Copying source odt..."
cp $DIR/template/template.odt template.odt
echo "Updating odt..."
zip -u template.odt content.xml
echo "Converting to docx..."
libreoffice --headless --convert-to "docx:Office Open XML Text" template.odt
echo "Converting to html..."
libreoffice --headless --convert-to "html:HTML" template.odt
echo "Converting to pdf..."
wkhtmltopdf template.html template-fromhtml.pdf
libreoffice --headless --convert-to "pdf:writer_pdf_Export" template.odt
echo "Finished..."
#libreoffice --headless --convert-to "docx:Office Open XML Text" myfile.html
#zip -u myfile.odt styles.xml
#zip -u myfile.odt styles.xml
#unzip -p myfile2.docx word/document.xml | sed "s/<w:tblStyle w:val=\"TableNormal\"/<w:tblStyle w:val=\"BlueTableStyle\"/g" > word/document.xml
#zip -u myfile2.docx word/document.xml

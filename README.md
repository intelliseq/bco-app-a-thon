# Documentation of the pipeline

### Starting the pipeline
To start the pipeline use:
```
docker run -it intelliseqngs/bco-app-a-thon
test.sh
```
Results of the pipeline are available in:
```
/results/
```

### Pipeline composition
Pipeline consists of two steps:
1. *fastqc*: running FastQC and generating quality check reports,
2. *alignment-bwa-mem*: alignment of reads to reference genome with BWA-MEM.

and additional step of generating the report.
Both steps produce the BCO files, which are joined (and validated) and from them, the report is generated (as pdf). If in the report there is included a *checksum*, the BCO file passed the in-pipeline validation.

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: BCO App-a-thon
#  version: v1.0
#  authors:
#      - https://gitlab.com/mateuszmarynowski
#      - https://gitlab.com/GlebLavr
#  copyright: Copyright 2019 Intelliseq
#  description: >
#  changes:
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

FROM intelliseqngs/java:openjdk12_v0.3

## reports
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y 

RUN apt-get install -y zip 
RUN apt-get install -y wget 
RUN apt-get install -y wkhtmltopdf 
RUN apt-get install -y libreoffice 
RUN apt-get install -y python3-pip 
RUN apt-get install -y fonts-lato 
RUN apt-get install -y locales
RUN apt-get install -y jo

RUN ln -s /usr/bin/python3 /usr/local/bin/python3 && \
    pip3 install jinja2
    
    
# generate raport script    
RUN mkdir -p /opt/tools && \
    wget -O /opt/tools/template.tar.gz \
      https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/resources/report-resources.tar.gz && \
    tar -xvzf /opt/tools/template.tar.gz -C /opt/tools && \
    chmod +x /opt/tools/generate-report.sh && \
    chmod +x /opt/tools/generate-report.py

# encoding
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8


# raport recources
RUN mkdir -p /resources
ADD https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/resources/report-resources.tar.gz /resources/report-resources.tar.gz


## BWA-MEM
RUN mkdir -p /opt/tools/bwa-mem
ADD https://github.com/GregoryFaust/samblaster/releases/download/v.0.1.24/samblaster-v.0.1.24.tar.gz /opt/tools/bwa-mem
ADD https://github.com/samtools/samtools/releases/download/1.8/samtools-1.8.tar.bz2 /opt/tools/bwa-mem
ADD https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2 /opt/tools/bwa-mem

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    build-essential \
    libz-dev \
    libncurses5-dev \
    libbz2-dev \
    liblzma-dev

# samblaster
RUN cd /opt/tools/bwa-mem && \
    tar -xvf /opt/tools/bwa-mem/samblaster-v.0.1.24.tar.gz  && \
    rm /opt/tools/bwa-mem/samblaster-v.0.1.24.tar.gz && \
    make -C /opt/tools/bwa-mem/samblaster-v.0.1.24/  && \
    ln -s /opt/tools/bwa-mem/samblaster-v.0.1.24/samblaster /usr/bin/samblaster

# samtools
RUN cd /opt/tools/bwa-mem && \
    tar -xvf /opt/tools/bwa-mem/samtools-1.8.tar.bz2 && \
    rm /opt/tools/bwa-mem/samtools-1.8.tar.bz2 && \
    make -C /opt/tools/bwa-mem/samtools-1.8  && \
    ln -s /opt/tools/bwa-mem/samtools-1.8/samtools /usr/bin/samtools

# bwa
RUN cd /opt/tools/bwa-mem && \
    tar -xvf /opt/tools/bwa-mem/bwa-0.7.17.tar.bz2&& \
    rm /opt/tools/bwa-mem/bwa-0.7.17.tar.bz2 && \
    make -C /opt/tools/bwa-mem/bwa-0.7.17 && \
    ln -s /opt/tools/bwa-mem/bwa-0.7.17/bwa /usr/bin/bwa

# bwa-mem resouces
RUN mkdir -p /resources/bwa-mem/Homo_sapiens_assembly/hg38
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.dict /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.dict
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa.amb /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa.amb
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa.ann /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa.ann
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa.bwt /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa.bwt
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa.fai /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa.fai
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa.pac /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa.pac
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa
ADD http://resouces.intelliseq.pl/public/intelliseqngs/workflows/resources/reference-genomes/broad-institute-hg38/only-chr15/Homo_sapiens_assembly38.fa.sa /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa.sa

## jq, nano
RUN echo "Running jq, nano apt-get"
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    jq \
    nano

## FastQC
RUN echo "Running fastqc apt-get"
RUN mkdir -p /opt/tools/fastqc && \
    wget -O /opt/tools/fastqc/fastqc_v0.11.7.zip http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.7.zip && \
    cd /opt/tools/fastqc && \
    unzip /opt/tools/fastqc/fastqc_v0.11.7.zip && \
    mv /opt/tools/fastqc/FastQC /opt/tools/fastqc/FastQC-v0.11.7 && \
    chmod +x /opt/tools/fastqc/FastQC-v0.11.7/fastqc && \
    ln -s /opt/tools/fastqc/FastQC-v0.11.7/fastqc /usr/bin/fastqc && \
    rm /opt/tools/fastqc/fastqc_v0.11.7.zip

## json with path to inputs
RUN mkdir /inputs/ && \
    JSON_STRING=$( jq -n \
                  --arg fastq_1 "/resources/fastq-files/282-chr15.47497301-49497301_1.fq.gz" \
                  --arg fastq_2 "/resources/fastq-files/282-chr15.47497301-49497301_2.fq.gz" \
                  '{"alignment.fastq_1": $fastq_1, "alignment.fastq_2": $fastq_2}' ) && \
    echo $JSON_STRING > /inputs/bco-app-a-thon-inputs.json

## cromwell jar file
RUN mkdir -p /cromwell/ && \
    wget https://github.com/broadinstitute/cromwell/releases/download/47/cromwell-47.jar -P cromwell/

## Resources
RUN mkdir -p /resources/fastq-files
ADD https://gitlab.com/intelliseq/workflows/raw/master/src/test/resources/data/fastq/282-chr15.47497301-49497301_1.fq.gz \
    /resources/fastq-files/282-chr15.47497301-49497301_1.fq.gz
ADD https://gitlab.com/intelliseq/workflows/raw/master/src/test/resources/data/fastq/282-chr15.47497301-49497301_2.fq.gz \
    /resources/fastq-files/282-chr15.47497301-49497301_2.fq.gz

## sh file to run pipeline    
ADD https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/scripts/test.sh \
    /usr/bin/test.sh
RUN chmod +x /usr/bin/test.sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  name: BWA-MEM alignment
#  authors:
#    - https://gitlab.com/marpiech,
#  copyright: Copyright 2019 Intelliseq
#  description: >
#    Short read alignment with bwa mem
#  changes:
#    latest:
#      - no changes
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow alignment_bwa_mem_workflow { call alignment_bwa_mem {}}

task alignment_bwa_mem {

  # @Input(name = "Fastq 1", desc = "Fastq file 1 (paired-end)")
  File fastq_1
  # @Input(name = "Fastq 2", desc = "Fastq file 2 (paired-end)")
  File fastq_2

  String task_name = "alignment_bwa_mem"
  String task_version = "latest"

  # Inputs with defaults
  # @Input(name = "Sample id")
  String sample_id = "no_id_provided"
  String filename = basename(fastq_1, "_1.fq.gz")

  # Tools runtime settings, paths etc.
  String RG_PL = "Illumina"
  String num_cpu = "8"


  command <<<
      task_name="${task_name}"; task_version="${task_version}"    
      source <(curl -s https://gitlab.com/intelliseq/workflows/raw/master/src/main/scripts/bco/v1/after-start.sh)

      zcat -f ${fastq_1} | head -1 | cut -d ':' -f 3,4 | sed 's/:/\./g' > rg_id
      RG_ID=`cat rg_id`
      RG_PU="$RG_ID"".""${sample_id}"
      RG_LB="${sample_id}"".library"
      RG_SM="${sample_id}"
      RG_PL="${RG_PL}"

      set -o pipefail
      set -e

      # Run BWA MEM, samblaster and samtools
      bwa mem \
        -t ${num_cpu} \
        -R "@RG\tID:""$RG_ID""\tPU:""$RG_PU""\tPL:${RG_PL}\tLB:""$RG_LB""\tSM:""$RG_SM" \
        -K 10000000 \
        -v 3 \
        -Y /resources/bwa-mem/Homo_sapiens_assembly/hg38/Homo_sapiens_assembly38.fa \
        ${fastq_1} ${fastq_2} \
          | sed 's/CL:bwa.*//' \
          | samblaster \
          | samtools sort -@ ${num_cpu} - > ${filename}.${sample_id}.markdup.bam
      
      
      wget -O bioobject.py https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/scripts/versions.py
      python3 bioobject.py "${task_name}"

      source <(curl -s https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/scripts/before-finish.sh)

  >>>

  runtime {

    memory: "16G"
    cpu: "8"

  }

  output {

    File lane_markdup_bam = "${filename}.${sample_id}.markdup.bam"

    # @Output(required=true,directory="/alignment_bwa_mem",filename="stdout.log")
    File stdout_log = stdout()
    # @Output(required=true,directory="/alignment_bwa_mem",filename="stderr.log")
    File stderr_log = stderr()
    # @Output(required=true,directory="/alignment_bwa_mem",filename="bioobject.json")
    File bco_bwa_mem = "bco.json"

  }

}

workflow bco_report_workflow { call bco_report {}}

task bco_report {

  File bco_json

  String task_name = "bco_report"
  String task_version = "latest"

  command <<<
  /opt/tools/generate-report.sh --json bco=${bco_json} --template /opt/tools/template/content.xml

  >>>

  runtime {

    memory: "500M"
    cpu: "1"

  }

  output {
    File bco_report_pdf = "template.pdf"
    File bco_report_odt = "template.odt"
    File bco_report_docx = "template.docx"

    # @Output(Required=True,Directory="/bco",Name="stdout.log")
    File stdout_log = stdout()
    # @Output(Required=True,Directory="/bco",Name="stderr.log")
    File stderr_log = stderr()
    # @Output(Required=True,Directory="/bco",Name="bioobject.json")
    #File bioobject = "bioobject.json"

  }

}

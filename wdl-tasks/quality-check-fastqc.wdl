# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  Name: FastQC quality check
#  Authors:
#    - https://gitlab.com/dzesikahoinkis
#    - https://gitlab.com/marysiaa
#    - https://gitlab.com/marpiech
#  Copyright: Copyright 2019 Intelliseq
#  Description: >
#    Runs FastQC and generates quality check reports.
#  Changes:
#    latest:
#      - no changes
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow quality_check_fastqc_workflow { call quality_check_fastqc {}}

task quality_check_fastqc {

  File fastq_1
  File fastq_2

  String task_name = "quality_check_fastqc"
  String task_version = "1.0"

  command <<<
    task_name="${task_name}"; task_version="${task_version}"
    source <(curl -s https://gitlab.com/intelliseq/workflows/raw/master/src/main/scripts/bco/v1/after-start.sh)

    mkdir quality-check

    fastqc -o quality-check ${fastq_1}
    fastqc -o quality-check ${fastq_2}


    wget -O bioobject.py https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/scripts/versions.py
    python3 bioobject.py "${task_name}"
  
    source <(curl -s https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/scripts/before-finish.sh)

  >>>

  runtime {

    memory: "4G"
    cpu: "1"

  }

  output {

    Array[File] fastqc_files = glob("quality-check/*")

    # @Output(required=true,directory="/quality_check_fastqc",filename="stdout.log")
    File stdout_log = stdout()
    # @Output(required=true,directory="/quality_check_fastqc",filename="stderr.log")
    File stderr_log = stderr()
    # @Output(required=true,directory="/quality_check_fastqc",filename="bioobject.json")
    File bco_fastqc = "bco.json"

  }

}

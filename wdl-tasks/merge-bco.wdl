workflow merge_bco_workflow { call merge_bco {} }

task merge_bco {
  File bco_fastqc
  File bco_bwa_mem
  # @Input(description="", required="false")
  String task_name = "merge_bco"
  String task_version = "1.0"

  command <<<
    provenance_domain=`jq -s '{ steps: map(.provenance_domain.steps[]) }' ${bco_fastqc} ${bco_bwa_mem}`
    echo_provenance_domain=`echo $provenance_domain | jq '.steps'`

    execution_domain=`jq -s '{ execution_domain: map(.execution_domain) }' ${bco_fastqc} ${bco_bwa_mem}`
    echo_execution_domain=`echo $execution_domain | jq '.execution_domain'`

    parametric_domain=`jq -s '{ parametric_domain: map(.parametric_domain[]) }' ${bco_fastqc} ${bco_bwa_mem}`
    echo_parametric_domain=`echo $parametric_domain | jq '.parametric_domain'`

    wget https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/description_domain.json
    description_domain=`cat description_domain.json | jq '.description_domain'`

    wget https://gitlab.com/intelliseq/bco-app-a-thon/raw/master/usability_domain.json
    usability_domain=`cat usability_domain.json | jq '.usability_domain'`

    biocomputeobject=$(jo -p\
      bco_spec_version="https://w3id.org/biocompute/1.3.0/" \
      bco_id="https://intelliseq.com/flow/fdb3091e-5420-46b9-b1f6-e6e88e94bac1" \
      provenance_domain=$(jo name=alignment version=1.0 steps="$echo_provenance_domain") \
      execution_domain="$echo_execution_domain" \
      parametric_domain="$echo_parametric_domain" \
      description_domain="$description_domain" \
      usability_domain="$usability_domain" \
    )

    echo "$biocomputeobject" > bco.json

  >>>

  runtime {

    memory: "500M"
    cpu: "1"

  }

  output {

    # @Output(keep=true, outdir="/tmp", filename="stdout.log", description="logs")
    File stdout_log = stdout()
    # @Output(keep=true, outdir="/tmp", filename="stderr.log")
    File stderr_log = stderr()
    # @Output(keep=true, outdir="/tmp", description="")
    File bco = "bco.json"

  }
}








